var development = require('./env/development')
var test = require('./env/test')
var testFront = require('./env/test-front')
var production = require('./env/production')


var env = {
  development: development,
  test: test,
  testFront: testFront,
  production: production
}[process.env.NODE_ENV || 'development'];

var MONGO_DB
var DOCKER_DB = process.env.DB_PORT
if ( DOCKER_DB ) {
  MONGO_DB = DOCKER_DB.replace( 'tcp', 'mongodb' ) + '/' + env.db
} else {
  MONGO_DB = process.env.MONGODB
}

console.log('>>>>>>>>>>>>>>>>');
console.log(MONGO_DB);
console.log(DOCKER_DB);
console.log('>>>>>>>>>>>>>>>>');

module.exports = {
  db: MONGO_DB
}
