'use strict'

var express = require('express')
var morgan = require('morgan')
var	bodyParser = require('body-parser')
var methodOverride = require('method-override')
var multer  = require('multer')

var env = process.env.NODE_ENV || 'development'

module.exports = function (app, passport) {

  app.all('*', function(req, res, next) {
  	res.header("Access-Control-Allow-Origin", "*");
  	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  	res.header("Access-Control-Allow-Headers", "X-Requested-With,Content-Type,Authorization,Cache-Control");
  	res.header("Access-Control-Expose-Headers", "id");
  	next();
  })

  app.use(express.static('assets'));
  if (env !== 'test') app.use(morgan('dev'));
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true })); // makes responses in nested hashes
  app.use(methodOverride('X-HTTP-Method-Override'));
  // Use the passport package in our application
  app.use(passport.initialize());
  if (env !== 'production') {
    app.locals.pretty = true;
  }
  app.use(multer({ dest: './assets/consentDocument'}));
}
