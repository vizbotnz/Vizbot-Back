var request = require('request')
var path = require('path')
var archiver = require('archiver')
var util = require('util')
var pdfFiller = require('pdffiller');

var services = require('../services/mongooseServices')
var createPDFDataMap = require('../services/pdf/create-pdf-data-map')


var documentUrl = 'http://ec2-52-18-99-146.eu-west-1.compute.amazonaws.com';

exports.getConsent = function(req, res){
	services.getConsentById(req.params.id, function(consent){
    if(consent)
     res.json(consent);
   else{
    res.status(404).end("Consent not avalaible");
  }
});
};

function addFileToArchive (archive, hashStoredName, filename) {
	var absolutePath = path.resolve(__dirname, '../assets/consentDocument/' + hashStoredName)
	archive.file(absolutePath, { name: filename })
}

exports.getConsentDocuments = function (req, res) {
	services.getConsentById(req.params.id, function(consent){
		if (consent) {
			console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
			console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
			console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
			console.log(util.inspect(consent, {showHidden: false, depth: null}));
			console.log('<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<');
			console.log('<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<');
			console.log('<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<');

			res.attachment('HCC_Consent_Application.zip')

			var archive = archiver('zip')
			archive.on('error', function (err) {
				res.status(500).send({error: err.message})
			})
			archive.on('end', function () {
				console.log('Zipped %d bytes', archive.pointer());
			})
			archive.pipe(res)

			var hccConsentApp = '../services/pdf/HCC-consent-app-form.PDF'
			var absoluteHccConsentApp = path.resolve(__dirname, hccConsentApp)
			// archive.file(absoluteHccConsentApp, { name: 'HCC-consent-app-form-complete.PDF'})

			consent.doc.forEach(function (document) {
				if (!document.url.includes('productspec')) {
					addFileToArchive(archive, document.url, document.name)
				}
			})
			var specSystemsPDFpath = path.resolve(__dirname, './../services/pdf/HCC_consent_application_specified_systems_section.PDF')
			archive.file(specSystemsPDFpath, {name: 'HCC-consent-application-specified-systems.pdf'})

			consent.compliance.buildingCodeClauses
				.filter(function (compliance) {
					return compliance.documentUrl
				})
				.forEach(function (complianceWithDoc) {
					addFileToArchive(archive, complianceWithDoc.documentUrl, complianceWithDoc.documentName)
				})


			var filledFormPath = path.resolve(__dirname, '../assets/filledHCC.pdf')
			var dataMap = createPDFDataMap(consent)

			pdfFiller.fillForm(absoluteHccConsentApp, filledFormPath, dataMap, function (err) {
				if (err) throw err
				archive.file(filledFormPath, {name: 'filledHCCForm.pdf'})
				archive.finalize()
			})

			// pdfFillService(consent, function (err, absolutePdfPath) {
				// if (err) throw err
				// archive.file(absolutePdfPath)
			// })
    } else {
      res.status(404).end("Consent not avalaible");
		}
  })
}

exports.getConsents = function(req, res){
  services.getConsents(req.params.id, function(consents){
    if(consents) {
      res.json(consents);
    } else {
      res.status(404).end("No new consents");
	  }
	});
};

exports.deleteConsent = function (req, res) {
	services.deleteConsent(req.params.id, function (statusCode) {
		if (statusCode == 204) {
			res.status(statusCode).end("Consent deleted");
		} else {
			res.status(statusCode).end("Unable to delete consent");
		}
	})
}

exports.getSubmissions = function(req, res){
  services.getSubmissions(function(submissionsList){
    if(submissionsList)
     res.json(submissionsList);
   else{
    res.status(404).end("No new consents");
  }
});
};

exports.getSubmission = function(req, res){
  services.getSubmission(req.params.id, function(submission){
    if(submission)
     res.json(submission);
   else{
    res.status(404).end("Consent not avalaible");
  }
});
};

exports.createConsent = function(req, res){
  if(req.body == null) res.status(400).end("Syntax error");
  else if(!req.body.title || !req.body.role || !req.body.address){
    res.status(400).end("Missing field");
  }else{
    services.getUserById(req.body.user, function(user){
      if(!user)
        res.status(400).end("User unknown :  unable to create a consent");
    });
    var consent = {
      user : req.body.user,
      title : req.body.title,
      role : req.body.role,
      address : req.body.address,
      status : req.body.status,
      councilRef : req.body.councilref,
      lawfullyUse : req.body.lawfullyUse,
      newUse : req.body.newUse,
      numberOccupants : req.body.numberOccupants,
      old : req.body.old,
      buildingWork : req.body.buildingWork,
			legalDescription : req.body.legalDescription,
			buildingInfo : {
				intendedLife : req.body.buildingInfo.intendedLife
			},
			project: {
				value : req.body.project.value,
			}
    };
    services.createConsent(consent, function(code, id, instance){
      if(code == 201){
        res.setHeader("url", req.url);
        res.setHeader("id", id);
        res.json(instance);
        res.status(code).end("Consent added");
      }
      else if(code == 409)
        res.status(code).end("Conflict : Unable to add Note");
    });
  }
};

exports.updateConsent = function(req, res){
  var id = req.params.id;
  if(req.body == null) res.status(400).end("Syntax error");
  else{
    services.updateConsent(id, req.body, function(code, consent){
      if(code == 200){
        res.json(consent);
        res.status(code).end("Consent updated");
      }
      else if(code == 404)
        res.status(code).end("Conflict : Unable to Update the Consent");
    });
  }
};

exports.addBuildingInfo = function(req, res){
  var id = req.params.id;
  if(req.body == null) res.status(400).end("Syntax error");
  else{
    var buildingInfo = {
      client : String,
      description : String,
      location : String,
      area : String,
      levels : Number
    }
    services.addBuildingInfo(id,buildingInfo,function(code){
      if(code == 404)
        res.status(code).end("Unable to modify user");
      else
        res.status(code).end("User modified");
    });
  }
};

exports.addDocument = function(req, res){
  var doc = {
    url : req.files.file.name,
    name : req.files.file.originalname,
    idUser : req.params.id
  };
  services.updatedDocument(doc, function(code){
    if(code == 404){
      res.status(code).end("Unable to upload the files");
    }else{
      res.status(code).end("Document uploaded");
    }
  });
  console.log(req.files);
};



exports.addProductSpec = function(req, res){
	console.log('REQUEST');
	console.log('REQUEST');
	console.log('REQUEST');
	console.log(req);
	console.log('REQUEST');
	console.log('REQUEST');
  var productArray = req.body;
  for (var i = 0; i < productArray.length; i++) {
    var doc = {
      url: productArray[i].url,
      name : productArray[i].name,
			idUser: req.params.id
    };
		console.log('PRODUCTSPEC ADDED ----------------------------------');
		console.log('PRODUCTSPEC ADDED ----------------------------------');
		console.log('PRODUCTSPEC ADDED ----------------------------------');
		console.log('PRODUCTSPEC ADDED ----------------------------------');
		console.log('productSpec doc: ', doc);
		console.log('PRODUCTSPEC ADDED ----------------------------------');
		console.log('PRODUCTSPEC ADDED ----------------------------------');
		console.log('PRODUCTSPEC ADDED ----------------------------------');
    services.downloadDocument(doc, function(code){
			console.log('ESCAPED');
			console.log('ESCAPED');
			console.log('ESCAPED');
			console.log('ESCAPED');
			console.log('ESCAPED');
			console.log('ESCAPED');
			console.log('ESCAPED');
			console.log('ESCAPED');
			console.log('ESCAPED');
      if(code === 404){
        res.status(code).end("Unable to upload the files");
      }else{
       // console.log(res);
        res.status(code).end("Document downloaded to server");
      }
    });
  }

};

exports.addCodeCompliance = function(req, res){
  var doc = {
    url : req.files.file.name,
    name : req.files.file.originalname,
    idUser : req.params.id,
    clause : req.params.clause
  };
  services.addCompliance(doc, function(code){
    if(code == 404){
      res.status(code).end("Unable to upload the files");
    }else{
      res.status(code).end("Document uploaded");
    }
  });
  res.status(200).end("");
};

exports.addRfc = function(req, res){
  var id = req.params.id;
  if(req.body === null) res.status(400).end("Syntax error");
  else{
    services.addRfc(id, req.body.rfc, function(code){
      if(code === 404)
        res.status(code).end("Unable to add this rfc");
      else
        res.status(code).end("Rfc added");
    });
  }
};

exports.addRfi = function(req, res){
  var id = req.params.id;
  if(req.body === null) res.status(400).end("Syntax error");
  else{
    services.addRfi(id, req.body.rfi, function(code){
      if(code === 404)
        res.status(code).end("Unable to add this rfi");
      else
        res.status(code).end("Rfc added");
    });
  }
};

exports.updateStatus = function(req, res){
  var id = req.params.id;
  console.log(req.body);
  if(req.body === null) res.status(400).end("Syntax error");
  else{
    services.updateStatus(id, req.body.status, function(code){
      if(code === 404)
        res.status(code).end("Unable to add this status");
      else
        res.status(code).end("Status added");
    });
  }
};

exports.getRfi = function(req, res){
  services.getRfi(req.params.id, function(rfi){
    if(rfi)
     res.json(rfi);
   else{
    res.status(404).end("RFI not avalaible");
  }
});
};

exports.getStatus = function(req, res){
  services.getStatus(req.params.id, function(status){
    if(status)
     res.json(status);
   else{
    res.status(404).end("Status not avalaible");
  }
});
};

exports.submissionAccepted = function(req, res){
  var id = req.params.id;
  services.submissionAccepted(id, function(code){
    if(code === 404)
      res.status(code).end("Unable to add this status");
    else
      res.status(code).end("Status added");
  });
};

exports.submissionDenied = function(req, res){
  var id = req.params.id;
  if(req.body === null) res.status(400).end("Syntax error");
  else{
    services.submissionDenied(id, req.body.description, function(code){
      if(code === 404)
        res.status(code).end("Unable to add this status");
      else
        res.status(code).end("Status added");
    });
  }
};
