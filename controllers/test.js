var fs = require('fs');
var path = require('path');

var user = require('./user')

exports.carefullyPurgeDbAndFiles = function (req, res) {
	if (process.env.NODE_ENV === 'testFront') {
		user.deleteAllUsersAndConsents(req, res)
		// deleteTestUploads()         //comment out to speed tests
		// deletePdfCreationFdfFiles() //comment out to speed tests
		res.status(200).end()
	} else {
		res.status(404).end('No way buddy')
	}
}

function deleteTestUploads () {
	fs.readdir(path.resolve(__dirname, './../assets/consentDocument/'), function (err, files) {
		if (err) throw err
		files.forEach(function (file) {
			fs.unlink(path.resolve(__dirname, './../assets/consentDocument/' + file), function (err) {
				if (err) throw err
				console.log('deleted: ', file);
			})
		})
	})
}

function deletePdfCreationFdfFiles () {
	fs.readdir(path.resolve(__dirname, './../'), function (err, files) {
		if (err) throw err
		files.forEach(function (file) {
			if (file.match(/.fdf/)) {
				fs.unlink(path.resolve(__dirname, './../' + file), function (err) {
					if (err) throw err
					console.log('deleted: ', file);
				})
			}
		})
	})
}
