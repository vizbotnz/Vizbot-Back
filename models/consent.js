var mongoose = require('mongoose');

var ConsentSchema = mongoose.Schema({
  // = mongooseServices.updateConsent
    title : String,//
    role : String,//
    address : String,//
    councilRef : String,//
    status : String,//
    user : String,//
    submitted : Boolean,//
    vettingDescription : String,//mongooseServices.submissionDenied
    consentNumber:  String,// nowhere in the app except here
    valuation : String,//
    legalDescription : String,//
    restrictedBuildingWork : String,
    lawfullyUse : { // mongooseServices.createConsent
      first : String,
      second : String
    },
    newUse : {//
      first : String,
      second : String
    },
    processing : [{// mongooseServices.updateStatus
      location: String,
      status: String,
      status_date: String,
      livedays: Number,
      suspendeddays: Number,
      totaldays: Number
    }],
    numberOccupants : Number,//
    old : Number,//
    buildingWork : String,//
    buildingInfo : {//
      name : String,
      address : String,
      locationWithinSite : String,
      area : String,
      level : String,
      affectedLevels : String,
      totalFloor : String,
      existingFloorArea : String,
      proposedFloorArea : String,
      speSystem : Boolean,
      speUpdate : String,
      speFollows : String,
      intendedLife : String,
      hasCulturalSignificance : Boolean,
      previouslyIssuedConsents : String,
      projectInformationMemorandum : [{
        matter : String,
        comments : String
      }]
    },
    project : {//
      info : Boolean,
      number : String,
      value : String
    },
    agent: {//
      name : String,
      address : {
        street : String,
        suburb : String,
        city : String,
        postCode : String
      },
      phone : String,
      mail : String,
      website : String,
      relationshipToOwner : String
    },
    client: {//mongooseServices.createConsent
      name : String,
      address : {
        street : String,
        suburb : String,
        city : String,
        postCode : String
      },
      phone : String,
      mail : String,
      website : String,
      proofOfOwnership: {
        certificateOfTitle: Boolean,
        lease: Boolean,
        agreementForSalePurchase: Boolean
      }
    },
    contact: {//
      name : String,
      address : {
        street : String,
        suburb : String,
        city : String,
        postCode : String
      },
      phone : String,
      mail : String,
      website : String
    },
    lbp: [{//
      name: String,
      classL : String,
      lbp : String,
      certificat : String,
      particularWork : String
    }],
    people : [{//
      type : { type: String },
      name : String,
      address : {
        street : String,
        suburb : String,
        city : String,
        postCode : String
      },
      phone : String,
      website : String,
      registration : String,
      mail : String,
      registrationNumber : String
    }],
    doc : [{//
      url : String,
      name : String
    }],
    docTypes : [{type: String}],
    riskGroupsAcceptableSolutions : [{type: String}],
    compliance : {
      buildingCodeClauses: [{//mongooseServices.addCompliance
        clause : String,
        meansOfCompliance : [],
        referenceOnDrawingsAndSpecificationsAndComments : String,
        reasonForWaiver : String,
        reasonForModification : String,
        documentUrl : String,
        documentName : String
      }],
      fireService : {
        buildingMeetsSection21AFireServiceActCriteria : String,
        criteraFullyApplyToWorkProposed : []
      },
      newComplianceScheduleAddressToBeHeld : String,
      existingComplianceScheduleNumber : String
    },
    more : {//
     authorization : String,
     signature : String,
     date : Date
   },
   notifications : [{//
     message : String,
     from : String,
     warning : String,
     action : String
   }],
   RFI:[{ // MongooseServices.addRfc
    rfi_id : String,
    location : String,
    details : String,
    response : String,
    accepted : String,
    created_by : String,
    date_letter_sent : String,
    date_of_response : String,
    date_signed_off : String,
    signed_off_by : String,
    building_code_clause : String,
    building_code_sub_clause : String
  }],
  workingDays : Number,//
  pointOfContact: {
    invoicing: String,
    correspondence: String
  },
  buildingConstentattachmentsChecklistChecked:[],
  generalAttachmentsChecklistChecked: []
})

module.exports  = mongoose.model('Consent', ConsentSchema);
