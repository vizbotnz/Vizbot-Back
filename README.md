# Express API server

## Recently merged the angular vizbot front app into the public folder of this app.
To install currently, run `npm i && cd public && npm i && bower install && cd ../`

## Deployment
with docker containers - one for the webapp, and a standard mongo container for the db
[good docker tutorial](https://nodejs.org/en/docs/guides/nodejs-docker-webapp/).  The container runs debian:jessie

### Local 
using `docker-compose up` to spin up linked docker containers
if you get an error with port 27017 you may need to kill the local mongod service:  
`mongo --eval "db.getSiblingDB('admin').shutdownServer()`

### Hosted - AWS  
- AWS account: `dev@vizbot.co.nz`


Access staging AWS EC2 instance:   
`vizbot-testing`:  
`ssh -i spreadkey.pem ubuntu@ec2-52-208-96-216.eu-west-1.compute.amazonaws.com -v`  

You may need to set correct permissions on pem file: `chmod 400 spreadkey.pem`

checkout the correct branch for the instance (staging/dev/production) - this changes the command run on dockerfile load - staging & production should run the app with the `forever` module, dev and local should run the app with `nodemon` 

run the app (if not already running) using `sudo docker-compose up`

#### STOPPING DOCKER-COMPOSE AND RESTARTING IT WILL DROP THE DATABASE!!!



`ls` to check if app has been cloned down, if not there run `git clone https://github.com/vizbotnz/Vizbot-Back.git`

in not running/first time install, use `docker-compose up` to spin up linked docker containers and start the app

setting up a server for the first time, must open ports and open firewall:  
```
sudo iptables -A PREROUTING -t nat -i eth0 -p tcp --dport 80 -j REDIRECT --to-port 8080
Next, we need to open the Linux firewall to allow connections on port 80:

sudo iptables -A INPUT -p tcp -m tcp --sport 80 -j ACCEPT
sudo iptables -A OUTPUT -p tcp -m tcp --dport 80 -j ACCEPT
```
from [node-api-EC2 tutorial](http://www.lauradhamilton.com/how-to-set-up-a-nodejs-web-server-on-amazon-ec2)

view the server logs using `forever logs app.js`?

Worth setting up Continuous Integration eg Travis CI

setup new EC2 instance  
`sudo yum install git nodejs npm --enablerepo=epel`  
[MongoDB setup guide](https://github.com/SIB-Colombia/dataportal-explorer/wiki/How-to-install-node-and-mongodb-on-Amazon-EC2)

## Architecture
Refactoring for testing and multiple environments following patterns used:
[Madhum's node-express-mongoose demo app](https://github.com/madhums/node-express-mongoose-demo)
- environment handling
- testing
- mailers if needed

## Testing
API testing [https://www.codementor.io/nodejs/tutorial/testing-express-apis-with-supertest](https://www.codementor.io/nodejs/tutorial/testing-express-apis-with-supertest)
- tape
- supertest

### To run tests:
`npm run local` start the app in docker
in another terminal window:  
`npm run enter-docker` to connect to the web app in docker  
in the docker container bash:
`npm test`

# ANGULAR 1 Client App

Consumes Vizbot-Back, an express-node-mongo API server

find in `public/`

`cd public` to `npm i`, `bower i`,run the tests and build the env with grunt

## Building different environments
~~local testing: `grunt buildLocal` ~~ 
~~production: `grunt buildProduction`  ~~
~~staging: `grunt`~~
mash: `grunt buildMash` - sets angular apiUrl to relative paths, now that it is hosted staticallly via express.

these grunt tasks create the environment config file, which sets the url to which the API calls are sent.  
they also compile the SASS into CSS.

## Testing


End-to-end using Jasmine and [Protractor](http://www.protractortest.org/#/)  
Make sure you've got Java installed to run the webdriver that manages the selenium server.  
`npm run t-setup-0` in one tab [if required] - updates webdriver
`npm run t-setup-1` in another tab   - runs selenium webdriver
~~`npm run t-setup-2` in another tab  ~~ not needed now apps are combined
`npm test` in another tab - runs protractor tests

