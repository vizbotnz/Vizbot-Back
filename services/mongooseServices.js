var services = require('../services/mongooseServices'),
    bcrypt = require('bcrypt-nodejs'),
    mongoose = require('mongoose'),
    uri = "mongodb://127.0.0.1:27017/Vizbot",
    User = require('../models/user.js'),
    Consent = require('../models/consent.js')

var request = require('request');


/**
* Seek the user with the given id
*/
exports.getUserById = function(id, callback){
  User.findOne({ _id: id }, function(err, user){
    if(!err){
      callback(user);
    }
    else
      return console.log(err);
  });
}

/**
* Check authentication
**/
exports.checkAuth = function(mail, password, callback){
  // db.on('error', console.error.bind(console, 'connection error:'));

}

/**
* Create a new user
*/
exports.createUser = function(user, callback){

  // db.on('error', console.error.bind(console, 'connection error:'));
  var instance = new User();

  instance.name = user.name;
  instance.mail = user.mail;
  instance.pwd = user.pwd;
  instance.registration = user.registration;
  instance.address = user.address;

  instance.consents = [];

  instance.save(function (err, user, affected) {
    if (err) {callback(409);}
    else {
      if(affected == 1) callback(201, user.id);
      else {callback(409);}
    }
  });
}

exports.deleteUser = function(userId, callback) {
  User.find({id: userId}).remove().exec(function (err, res) {
    if (err) throw err
    callback(200)
  })
}

exports.deleteConsent = function(consentId, callback) {
  Consent.find({id: consentId}).remove().exec(function (err, res) {
    if (err) throw err
    callback(200)
  })
}


/**
* LogIn
*/
exports.logIn = function(mail, pwd, callback){
  // db.on('error', console.error.bind(console, 'connection error:'));
  console.log(mail);
  console.log(pwd);
  User.findOne({ mail: mail}, function(err, user){
    if(!err && user){
      callback(user.id, 200);
    }
    else{
      if (err) console.log(err);
      callback(undefined, 404);
    }
  });
}

/**
* Create consent
*/
exports.createConsent = function(consent, callback){
  // // db.on('error', console.error.bind(console, 'connection error:'));

  var ConsentModel = mongoose.model('Consent', Consent);

  var instance = new ConsentModel();
  var user = new User();

  instance.user = consent.user;
  instance.title = consent.title;
  instance.role = consent.role;
  instance.address = consent.address;
  instance.status = consent.status;
  instance.councilRef = consent.councilRef;
  instance.lawfullyUse = consent.lawfullyUse;
  instance.newUse = consent.newUse;
  instance.numberOccupants = consent.numberOccupants;
  instance.old = consent.old;
  instance.buildingWork = consent.buildingWork;
  instance.project.value = consent.project.value;
  instance.legalDescription = consent.legalDescription;
  instance.buildingInfo.intendedLife = consent.buildingInfo.intendedLife;

  instance.save(function (err, consent) {
    if (err) {
      console.log(err);
      callback(409);
    }
    else {
      User.findOne({ _id: consent.user }, function(err, user){
        if(!err && user){
          user.consents.push(consent.id);
          user.save(callback);
          if(consent.role === "Agent"){
            instance.agent = {
              name : user.name,
              address : user.address,
              phone : user.phone,
              mail : user.mail
            };
          }else if(consent.role === "Client/Owner"){
            instance.client = {
              name : user.name,
              address : user.address,
              phone : user.phone,
              mail : user.mail
            };
          }
          instance.save(callback);
          callback(201, consent.id, instance);
        }
        else{
          if (err) console.log(err);
          callback(undefined, 404);
        }
      });
    }
  });

}

/**
* Update Consent
*/
exports.updateConsent = function(id, consent, callback){ // does this contain every field
  // db.on('error', console.error.bind(console, 'connection error:'));
  var ConsentModel = mongoose.model('Consent', Consent);
  this.getConsentById(id, function(instance){
    Object.keys(consent).forEach(function (consentProperty) {
      instance[consentProperty] = consent[consentProperty]
    })
    instance.save(function(err,instance){
      if(err){
        console.log("Couldn't save consent: ", err);
        callback(404);
      }
      else
        callback(200);
    });
  });
}

/*
* Get consent by ID
*/

exports.getConsentById = function(id, callback){
  // db.on('error', console.error.bind(console, 'connection error:'));

  var ConsentModel = mongoose.model('Consent', Consent);
  ConsentModel.findOne({ _id: id }, function(err, consent) {
    if (!err) {
      console.log(consent);
      callback(consent);
    }
    else
      return console.log("Real error : "+ err);
  });
}


/*
* Get submissions list
*/
exports.getSubmissions = function(callback){
  // db.on('error', console.error.bind(console, 'connection error:'));

  var ConsentModel = mongoose.model('Consent', Consent);
  ConsentModel.find({ status: 'submitted' }, {_id : 1}, function(err, list){
    if(!err){
      console.log(list);
      callback(list);
    }
    else
      return console.log("No list : "+ err);
  });
}

/*
* Get submission by ID
*/
exports.getSubmission = function(id, callback){
  // db.on('error', console.error.bind(console, 'connection error:'));

  var ConsentModel = mongoose.model('Consent', Consent);
  ConsentModel.findOne({ submitted: false, _id: id}, function(err, submission){
    if(!err){
      callback(submission);
    }
    else
      return console.log("Real error : "+ err);
  });
}


/**
* Get all consents by user
*/
exports.getConsentsByUser = function(idUser, callback){
  // db.on('error', console.error.bind(console, 'connection error:'));

  var ConsentModel = mongoose.model('Consent', Consent);

  var instance = new ConsentModel();
  var consents = new Array();

  this.getUserById( idUser, function (user) {
    if (user.consents.length) {
      var consentCount = user.consents.length;
      for (var i = 0; i < consentCount ; i++) {
        services.getConsentById(user.consents[i], function(consent){
          consents.push(consent);
          if(consents.length === consentCount) callback(consents);
        });
      }
    }else{
      callback([]);
    }
  });
}

exports.updatedDocument = function(doc, callback){
  // db.once('error', console.error.bind(console, 'connection error:'));

  var ConsentModel = mongoose.model('Consent', Consent);
  var instance = new ConsentModel();
  var obj = {
    url : doc.url,
    name : doc.name
  };
  console.log(obj);
  ConsentModel.findOne({ _id: doc.idUser }, function(err, consent){
    if(!err && consent){
    //console.log(consent);
    consent.doc.push(obj);
    consent.save();
    callback(200);
  }
  else{
    if (err) console.log(err);
  }
});
}

exports.downloadDocument = function (doc, callback) {
  var file = fs.createWriteStream('')
  updateDocument(doc, function (statusCode) {
    callback(statusCode)
  })

}

exports.addCompliance = function(doc, callback){
  // db.once('error', console.error.bind(console, 'connection error:'));

  var ConsentModel = mongoose.model('Consent', Consent);
  // var instance = new ConsentModel();
  var obj = {
    documentUrl : doc.url,
    documentName : doc.name,
    clause : doc.clause
  };
  console.log('**doc**: ', doc);
  ConsentModel.findOne({ _id: doc.idUser }, function(err, consent){
    if(!err && consent){
      // compliance : {
      //   buildingCodeClauses: [{//mongooseServices.addCompliance
      //     clause : String,
      //     meansOfCompliance : [],
      //     referenceOnDrawingsAndSpecificationsAndComments : String,
      //     reasonForWaiver : String,
      //     reasonForModification : String
      //   }],
      consent.compliance.buildingCodeClauses.push(obj);
      consent.save();
      callback(201);
    }
    else{
      if (err) console.log(err);
    }
  });
}


exports.addRfc = function(id, rfc, callback){
  // db.once('error', console.error.bind(console, 'connection error:'));

  var ConsentModel = mongoose.model('Consent', Consent);
  var instance = new ConsentModel();

  ConsentModel.findOne({ _id: id }, function(err, consent){
    if(!err && consent){
      consent.RFI.push(rfc);
      consent.save();
      callback(201);
    }
    else{
      if (err) console.log(err);
    }
  });
}

exports.addRfi = function(id, rfi, callback){
  // db.once('error', console.error.bind(console, 'connection error:'));

  var ConsentModel = mongoose.model('Consent', Consent);
  var instance = new ConsentModel();

  ConsentModel.findOne({ _id: id }, function(err, consent){
    if(!err && consent){
      consent.RFI = rfi;
      consent.save();
      callback(201);
    }
    else{
      if (err) console.log(err);
    }
  });
}

exports.updateStatus = function(id, status, callback){
  // db.once('error', console.error.bind(console, 'connection error:'));

  var ConsentModel = mongoose.model('Consent', Consent);
  var instance = new ConsentModel();

  ConsentModel.findOne({ _id: id }, function(err, consent){
    if(!err && consent){
      console.log(status);
      consent.processing.push(status);
      consent.status = status.status;
      consent.save();
      console.log(consent);
      callback(201);
    }
    else{
      if (err) console.log(err);
    }
  });
}

/*
* Get RFI
*/
exports.getRfi = function(id, callback){
  // db.on('error', console.error.bind(console, 'connection error:'));

  var ConsentModel = mongoose.model('Consent', Consent);
  ConsentModel.find({_id: id}, {RFI: 1}, function(err, rfi){
    if(!err){
      callback(rfi);
    }
    else
      return console.log("Real error : "+ err);
  });
}

/*
* Get status
*/
exports.getStatus = function(id, callback){
  // db.on('error', console.error.bind(console, 'connection error:'));

  var ConsentModel = mongoose.model('Consent', Consent);
  ConsentModel.findOne({_id : id}, function(err, consent){
    if(!err){
      callback(consent.processing);
    }
    else
      return console.log("Real error : "+ err);
  });
}

/*
* Change vetting status  Accepted
*/

exports.submissionAccepted = function(id, callback){
  // db.once('error', console.error.bind(console, 'connection error:'));

  var ConsentModel = mongoose.model('Consent', Consent);

  ConsentModel.findOne({ _id: id }, function(err, consent){
    if(!err && consent){
      consent.submitted = true;
      consent.status = "vetting";
      consent.save();
      callback(201);
    }
    else{
      if (err) console.log(err);
    }
  });
}

/*
* Change vetting status Denied
*/

exports.submissionDenied = function(id, description, callback){
  // db.once('error', console.error.bind(console, 'connection error:'));

  var ConsentModel = mongoose.model('Consent', Consent);

  ConsentModel.findOne({ _id: id }, function(err, consent){
    if(!err && consent){
      consent.submitted = false;
      consent.vettingDescription = description;
      consent.save();
      callback(201);
    }
    else{
      if (err) console.log(err);
    }
  });
}

//module.exports = mongoose.model('User', schemas.userSchema);

exports.deleteAllUsersAndConsents = function(callback) {
  var ConsentModel = mongoose.model('Consent', Consent);
  var UserModel = mongoose.model('User', User);
  ConsentModel.find()
    .remove()
    .exec(function (err) {
      if (err) throw err
      UserModel.find()
        .remove()
        .exec(function (err) {
          if (err) throw err
          callback()
        })
    })
}
