var objectPath = require('object-path');

var fieldToDataMap = require('./fieldToDataMap');

module.exports = function (consent) {
  var dataMap = {}
  Object.keys(fieldToDataMap).forEach(function (key) {
    var fieldData
    if (typeof(fieldToDataMap[key]) === 'function') {
      fieldData = fieldToDataMap[key](consent)
    } else if (!parseInt(fieldToDataMap[key]) && fieldToDataMap[key] !== '') {
      fieldData = objectPath.get(consent, fieldToDataMap[key]) || 'noData'
    } else {
      fieldData = fieldToDataMap[key]
    }
    dataMap[key] = fieldData ? fieldData.toString() : ''
  })
  return dataMap
}
