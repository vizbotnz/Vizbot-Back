

var	errorhandler = require('errorhandler'),
	http = require('http'),
	https = require('https'),
	static = require('node-static'),
	request = require('request'),
	passport = require('passport'),
	express = require('express'),
	mongoose = require('mongoose')

var	authController = require('./controllers/auth'),
	user = require('./controllers/user'),
	consent = require('./controllers/consent'),
	testController = require('./controllers/test'),
	mongo = require('./services/mongooseServices'),
	config = require('./config')

var	app = express();

require('./config/express')(app, passport)

module.exports = app

app.set('port', process.env.PORT || 8080);

app.delete('/testDbPurge', testController.carefullyPurgeDbAndFiles)
app.get('/users/:id', user.getUser);
app.put('/users', authController.isAuthenticated, user.logIn);
app.post('/users', user.createUser);
app.put('/users/:id', user.modifyUser);
app.delete('/users/:id', user.deleteUser);
app.get('/users/:id/consents', user.getConsents);

app.get('/consents/:id', authController.isAuthenticated, consent.getConsent);
app.post('/consents', consent.createConsent);
app.post('/consents/:id' ,consent.updateConsent);
app.delete('/consents/:id', consent.deleteConsent)
app.post('/consents/:id/document', consent.addDocument);
app.post('/consents/:id/productspec', consent.addProductSpec);
app.post('/consents/:id/codeCompliance/:clause', consent.addCodeCompliance);
//app.get('/consents', consent.getConsents);
app.post('/consents/:id/rfc', authController.isAuthenticated, consent.addRfc);
app.post('/consents/:id/rfi', authController.isAuthenticated, consent.addRfi);
app.post('/consents/:id/status',authController.isAuthenticated, consent.updateStatus);
app.get('/consents/:id/rfi',authController.isAuthenticated, consent.getRfi);
app.get('/consents/:id/status',authController.isAuthenticated, consent.getStatus);
//app.post('/consents/:id/submitted', consent.consentSubmitted);
app.post('/consents/:id/productspec',authController.isAuthenticated, consent.addProductSpec);
app.get('/consents/:id/consentDocuments', consent.getConsentDocuments)

app.get('/submissions',authController.isAuthenticated, consent.getSubmissions);
app.get('/submissions/:id',authController.isAuthenticated , consent.getSubmission);
app.post('/submissions/:id/accepted',authController.isAuthenticated ,consent.submissionAccepted);
app.post('/submissions/:id/denied', authController.isAuthenticated, consent.submissionDenied);

connect()
  .on('error', console.log)
  .on('disconnected', connect)
  .once('open', listen);

function listen () {
  if (app.get('env') === 'test') return;
  app.listen(app.get('port'));
	console.log('Express app started on port ' + app.get('port'));
	// var credentials = {
	// 	key: fs.readFileSync('pathtokey/server.key', 'utf8'),
	// 	cert: fs.readFileSync('pathtocert/server.cert', 'utf8')
	// }
	// https.createServer(credentials, app).listen(443)
	// console.log('secure Express app started on port 443');
}

function connect () {
  return mongoose.connect(config.db).connection;
}
