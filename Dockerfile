FROM node:latest

RUN apt-get update &&\
    apt-get -y install pdftk &&\
    apt-get clean -y && apt-get autoclean -y && apt-get autoremove -y &&\
    npm i -g nodemon &&\
    mkdir -p /usr/src/app

WORKDIR /usr/src/app
COPY . /usr/src/app

ENV OWNER_PW=owner USER_PW=user
CMD pdftk - output - owner_pw $OWNER_PW user_pw $USER_PW

EXPOSE 8080

RUN npm install
CMD ["npm", "run", "test-frontend"]
