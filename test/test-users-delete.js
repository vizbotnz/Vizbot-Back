'use strict';

var mongoose = require('mongoose');
var request = require('supertest');
var test = require('tape');

var app = require('./../app');
var helper = require('./helper');

var User = mongoose.model('User');

var user
var _user = {
    name : 'Susan Strong',
    mail : 'verybuff@gmail.com',
    pwd : 'ilovefinn'
}
test('*Clean up*', helper.cleanup);

test('Create user', function(t){
  user = new User(_user);
  user.save(t.end)
})

test('DELETE user', function (t) {
  request(app)
    .delete('/users/' + user.id)
    .end(function (err, res) {
      t.error(err, 'No error');
      t.same(res.status, 200, 'Correct response returned');
      User.findOne({id: user.id}).exec(function (err, user) {
        if (err) throw err
        t.same(user, null, 'User deleted from DB')
      })
      t.end();
    });
})

test('*Clean up*', helper.cleanup);
