'use strict';

var mongoose = require('mongoose');
var request = require('supertest');
var test = require('tape');
var _ = require('lodash')
var traverse = require('traverse');
var getIn = require('get-in')

var app = require('./../app');
var helper = require('./helper');
var _hccConsent = require('./data/create-consent-data')
var _hccConsentUpdates = require('./data/update-complete-consent-data')

var Consent = mongoose.model('Consent');
var User = mongoose.model('User');

var user
var consent
var _user = {
  name : 'Susan Strong',
  mail : 'verybuff@gmail.com',
  pwd : 'ilovefinn'
}

test('Clean up',helper.cleanup);

test('Create user and consent',function (t) {
    user = new User(_user);
    user.save(function (res) {
      _hccConsent.user = user.id
      consent = new Consent(_hccConsent)
      consent.save(function (res) {
        _hccConsentUpdates.id = consent.id
        _hccConsentUpdates.user = user.id
      })
      t.end();
    });
})

// app.post('/consents/id', consent.updateConsent)

test('POST consent/id', function (t) {
  request(app)
    .post('/consents/' + consent.id)
    .type('form')
    .send(_hccConsentUpdates)
    .end(function (err, res) {
      t.error(err, 'No error');
      t.equal(res.status, 200, 'Correct response status');
      Consent.count( function(err, c) {
        t.equal(c, 1, 'One consent is in the database');
      })
      Consent.findOne()
        .exec(function (err,instance) {
          assertExpectedIsSubset(t, instance, _hccConsentUpdates)
        })
      t.end();
    });
})

function assertExpectedIsSubset (assert, actual, expected) {
 traverse(expected).forEach(function (expectedNode) {
   var actualNode = getIn(actual, this.path)
   if (this.isLeaf) {
     assert.same(actualNode, expectedNode, this.path + ' should be equal')
   }
 })
}

test('Clean up', helper.cleanup);
