module.exports = {
  user : undefined,
  title : 'House of Sam',
  role : 'Client/Owner',
  address : '123 sam street, samtown',
  status : '?',
  councilRef : '?',
  lawfullyUse : {
    first: "?house",
    second: "?detatched"
  },
  newUse : {
    first: "?apartment",
    second: "?communal"
  },
  old : 1892,
  buildingInfo: {
    intendedLife: 23
  },
  numberOccupants : 32,
  buildingWork : "converting to communal housing, linking detached dwellings",
  project: {
    value : "102000"
  },
  legalDescription : "Lot 4 DP 12039"

}
