'use strict';

var mongoose = require('mongoose');
var request = require('supertest');
var test = require('tape');

var app = require('./../app');
var helper = require('./helper');

var Consent = mongoose.model('Consent');
var User = mongoose.model('User');

var user
var hccConsent = require('./data/create-consent-data')

var _user = {
    name : 'Susan Strong',
    mail : 'verybuff@gmail.com',
    pwd : 'ilovefinn'
}


test('*Clean up*', helper.cleanup);

test('Create user',function (t) {
    user = new User(_user);
    user.save(function () {
      hccConsent.user = user.id
      t.end();
    });
})

// app.post('/consents', consent.createConsent)

test('POST consent', function (t) {
  request(app)
    .post('/consents/')
    .type('form')
    .send(hccConsent)
    .end(function (err, res) {
      t.error(err, 'No error');
      t.equal(res.status, 200, 'Correct response status');
      Consent.count({title: 'House of Sam'}, function(err, c) {
        t.equal(c, 1, 'One consent was created');
      });
      Consent.count( function(err, c) {
        t.same(c, 1, 'One consent is in the database');
      });
      t.end();
    });
})

test('*Clean up*', helper.cleanup);

test.onFinish(() => process.exit(0));
