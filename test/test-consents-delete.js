'use strict';

var mongoose = require('mongoose');
var request = require('supertest');
var test = require('tape');

var app = require('./../app');
var helper = require('./helper');

var Consent = mongoose.model('Consent');
var User = mongoose.model('User');

var user
var hccConsent = {
  user : undefined,
  title : 'House of Sam',
  role : 'Client/Owner',
  address : '123 sam street, samtown',
  status : '?',
  councilRef : '?',
  lawfullyUse : {
    first: "?house",
    second: "?detatched"
  },
  newUse : {
    first: "?apartment",
    second: "?communal"
  },
  numberPeople : 32,
  old : 1892,
  buildingWork : "converting to communal housing, linking detached dwellings",
  valuation : "102000",
  legalDescription : "Lot 4 DP 12039"

}
var _user = {
    name : 'Susan Strong',
    mail : 'verybuff@gmail.com',
    pwd : 'ilovefinn'
}


test('*Clean up*', helper.cleanup);

test('Create user',function (t) {
    user = new User(_user);
    user.save(function () {
      hccConsent.user = user.id
      t.end();
    });
})

// app.post('/consents', consent.createConsent)

test('DELETE consent', function (t) {
  request(app)
    .delete('/consents/' + hccConsent.id)
    .end(function (err, res) {
      t.error(err, 'No error');
      t.equal(res.status, 200, 'Correct response status');
      Consent.findOne({title: 'House of Sam'}, function(err, c) {
        t.equal(c, null, 'Consent deleted');
      });
      t.end();
    });
})

test('*Clean up*', helper.cleanup);

// test.onFinish(() => process.exit(0));
