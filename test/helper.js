'use strict';

var request = require('supertest');
const mongoose = require('mongoose');
var test = require('tape');
const co = require('co');

var app = require('./../app');
var express = require('./../app');

const User = mongoose.model('User');
const Consent = mongoose.model('Consent');

exports.cleanup = function (t) {
  co(function* () {
    yield User.remove();
    yield Consent.remove();
    t.end();
  });
}

exports.closeConnection = function (t) {
  mongoose.disconnect();
  t.pass('closing db connection');
  t.end()
}
