var test = require('tape')

var updatedConsentData = require('./data/update-complete-consent-data')
var completedPdfDataMap = require('./data/completed-pdf-data-map')

var createPdfDataMap = require('./../services/pdf/create-pdf-data-map')

test('Correct PDF dataMap created', function (t) {
  var dataMap = createPdfDataMap(updatedConsentData)
  Object.keys(dataMap).forEach(function (key) {
    t.equal(dataMap[key], completedPdfDataMap[key], key + ' should be correct')
  })
  t.end()
})
